package com.example.kananyusub.weatherapp.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.kananyusub.weatherapp.R;
import com.example.kananyusub.weatherapp.api.ApiInitHelper;
import com.example.kananyusub.weatherapp.model.ApiResponseModel;
import com.example.kananyusub.weatherapp.model.Data;
import com.example.kananyusub.weatherapp.ui.adapter.WeatherAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recycler_weather)
    RecyclerView mRecyclerView;
    private List<Data> mDataList;
    private WeatherAdapter mAdapter;
    private ApiInitHelper mApiInitHelper;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mApiInitHelper = ApiInitHelper.getInstance();
        mDataList = new ArrayList<>();
        getWeather();
    }

    private void getWeather() {
        Call<ApiResponseModel> call = mApiInitHelper
                .getApi()
                .getResult("0.409264,49.867092", "az");

        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this,
                            "Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }

                ApiResponseModel apiResponse = response.body();

                for (int i = 0; i < apiResponse.getHourly().getData().size(); i++) {
                    mDataList.add(apiResponse.getHourly().getData().get(i));
                }

                initRecyclerView();

            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new WeatherAdapter(MainActivity.this, mDataList);
        mRecyclerView.setAdapter(mAdapter);

    }
}
