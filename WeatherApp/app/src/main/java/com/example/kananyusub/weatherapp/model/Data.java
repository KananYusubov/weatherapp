package com.example.kananyusub.weatherapp.model;

import com.example.kananyusub.weatherapp.R;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("time")
    private long mTime;
    @SerializedName("summary")
    private String mSummary;
    @SerializedName("icon")
    private String mIcon;
    @SerializedName("temperature")
    private String mTemperature;

    public String getSummary() {
        return mSummary;
    }

    public int getIcon() {
        switch (mIcon) {
            case "clear-day":
                return R.drawable.ic_clear_day;
            case "clear-night":
                return R.drawable.ic_clear_day;
            case "wind":
                return R.drawable.ic_clear_day;
            case "rain":
                return R.drawable.ic_rain;
            case "cloudy":
                return R.drawable.ic_cloudy;
            case "partly-cloudy-day":
                return R.drawable.ic_partly_cloudy_day;
            case "partly-cloudy-night":
                return R.drawable.ic_partly_cloudy_night;
        }
        return R.drawable.ic_clear_day;
    }

    public String getTemperature() {
        return mTemperature;
    }

    public String getTime() {
        long hour = (mTime / (60 * 60)) % 24;
        long minute = (mTime / 60) % 60;
        String time = String.format("%02d: %02d", hour, minute);
        return time;
    }

    public int getItemType() {
        switch (mIcon) {
            case "clear-day":
            case "clear-night":
            case "wind":
                return 0;

            case "rain":
            case "cloudy":
            case "partly-cloudy-day":
            case "partly-cloudy-night":
                return 1;
        }
        return 0;
    }
}
