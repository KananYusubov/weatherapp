package com.example.kananyusub.weatherapp.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kananyusub.weatherapp.R;
import com.example.kananyusub.weatherapp.model.Data;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_CLEAR = 0;
    private static final int VIEW_TYPE_CLOUDY = 1;

    private Context mContext;
    private List<Data> mDataList;

    public WeatherAdapter(Context context, List<Data> dataList) {
        mContext = context;
        mDataList = dataList;
    }

    @Override
    public int getItemViewType(int position) {
        int type = mDataList.get(position).getItemType();

        if (type == 0) {
            return VIEW_TYPE_CLEAR;
        } else if (type == 1) {
            return VIEW_TYPE_CLOUDY;
        }

        return 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_CLEAR:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_clear_day, parent, false);
                return new NonCloudyHolder(mContext, view, mDataList);

            case VIEW_TYPE_CLOUDY:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_cloudy_day, parent, false);
                return new CloudyHolder(mContext, view, mDataList);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_TYPE_CLEAR:
                NonCloudyHolder nonCloudyAdapter = (NonCloudyHolder) holder;
                nonCloudyAdapter.bindView(position);
                break;
            case VIEW_TYPE_CLOUDY:
                CloudyHolder cloudyHolder = (CloudyHolder) holder;
                cloudyHolder.bindView(position);
                break;

        }
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

}
