package com.example.kananyusub.weatherapp.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiInitHelper {
    private static final String BASE_URL = "https://api.darksky.net" +
            "/forecast/390f04b894ad4ede0ba506c11fefdc2d/";

    private static ApiInitHelper mInstance;
    private Retrofit mRetrofit;

    private ApiInitHelper() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();
                        Request newRequest = originalRequest.newBuilder()
                                .header("Interceptor-Header", "test_header")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .addInterceptor(loggingInterceptor)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized ApiInitHelper getInstance() {
        if (mInstance == null) {
            mInstance = new ApiInitHelper();
        }
        return mInstance;
    }

    public ApiMethods getApi() {
        return mRetrofit.create(ApiMethods.class);
    }
}
