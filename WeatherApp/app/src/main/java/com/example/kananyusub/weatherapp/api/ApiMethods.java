package com.example.kananyusub.weatherapp.api;

import com.example.kananyusub.weatherapp.model.ApiResponseModel;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiMethods {

    @GET("{coord}/")
    Call<ApiResponseModel> getResult(
            @Path("coord") String cord,
            @Query("lang") String lang);

    /*
    Just to learn (Source:: coding in flow youtube channel)

    *** GET REQUEST ***

   1) @GET(posts/{id}/comments)
    Call<List<Comments>> getComments(@Path("id") int postId);

   2) @GET("posts")
   Call<List<Posts>> getPosts(@Query("userId") int userId);

   3) @GET("posts")
   Call<List<Posts>> getPosts(@QueryMap Map<String,String> parameters)

   4) @GET("posts")
   Call<List<Posts>> getPosts(@Url String url);

    *** POST REQUEST, FORMURLENCODED

   5) @POST
   Call<Post> createPost(@Body Post post);

   6) @FormUrlEncoded
      @Post
      Call<Post> createPost(
      @Field("userId") int userId,
      @Field("title") String title
      );

      7) @FormUrlEncoded
      @Post
      Call<Post> createPost(
      @FieldMap Map<String, String> fields);
      );

      8)
      @Headers({"Static-Header1: 123", "Static-Header2: 456"})
      @PUT("posts/{id}")
   Call<Post> updatePost(
   @Header("Dynamic-Header") String header,
   @Path("id") int id , @Body Post post);

      9) @PATCH("posts/{id}")
   Call<Post> updatePost(@Path("id") int id , @Body Post post);
     */


}
