package com.example.kananyusub.weatherapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.kananyusub.weatherapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FooterHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progress_circular)
    ProgressBar mProgressBar;

    public FooterHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
