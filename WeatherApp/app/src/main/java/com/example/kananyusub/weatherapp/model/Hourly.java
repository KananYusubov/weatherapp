package com.example.kananyusub.weatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hourly {
    @SerializedName("data")
    private List<Data> mData;

    public List<Data> getData() {
        return mData;
    }
}
