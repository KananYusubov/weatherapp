package com.example.kananyusub.weatherapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kananyusub.weatherapp.R;
import com.example.kananyusub.weatherapp.model.Data;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CloudyHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private List<Data> mDataList;
    @BindView(R.id.image_weather_icon)
    ImageView mImageViewIcon;
    @BindView(R.id.text_weather_content)
    TextView mTextViewContent;

    public CloudyHolder(Context context, View itemView, List<Data> dataList) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mContext = context;
        mDataList = dataList;
    }

    void bindView(int position) {

        Data currentData = mDataList.get(position);
        mImageViewIcon.setImageResource(currentData.getIcon());

        String content = "";

        content += "<b>" + mContext.getString(R.string.summary) + " </b>"
                + currentData.getSummary() + "<br><br>";

        content += "<b>" + mContext.getString(R.string.time) + " </b>"
                + currentData.getTime() + "<br>";

        content += "<b>" + mContext.getString(R.string.temperature) + " </b>"
                + currentData.getTemperature() + "<br>";

        mTextViewContent.setText(Html.fromHtml(content));

    }
}
